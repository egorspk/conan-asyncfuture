#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools
from conans.errors import ConanException


class AsyncfutureConan(ConanFile):
    name = "asyncfuture"
    version = "0.4.1"
    homepage = "https://github.com/benlau/asyncfuture"
    url = "https://bitbucket.org/egorspk/conan-asyncfuture"
    description = "Conan.io package for asyncfuture."

    def source(self):
        url = "https://github.com/benlau/asyncfuture/archive/v0.4.1.zip"
        tools.get(url)

    def package(self):
        self.copy(pattern="*asyncfuture.h", dst="include", keep_path=False)

    def package_id(self):
        self.info.header_only()
