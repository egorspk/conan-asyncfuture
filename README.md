# conan-asyncfuture

The packages generated with this **conanfile** can be found in [bintray.com](https://bintray.com/spk/conan).

## Reuse the package

### Basic setup

```
conan remote add spk https://api.bintray.com/conan/spk/conan 
conan install asyncfuture/0.4.1@spk/stable
```
